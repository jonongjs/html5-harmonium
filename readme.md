# [HTML5 Harmonium](https://bitbucket.org/jonongjs/html5-harmonium/)

HTML5 Harmonium is a music doodling tool inspired by MetaFight's Wii homebrew project, [Harmonium](http://wiibrew.org/wiki/Harmonium), which in turn was inspired by André Michelle's [music doodling tool](http://lab.andre-michelle.com/tonematrix).

## License

### Major components:

* HTML5 Boilerplate: Public domain
* jQuery: MIT/GPL license
* Modernizr: MIT/BSD license
* Normalize.css: Public Domain

### Everything else:

The Unlicense (aka: public domain)
