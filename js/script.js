/*
Author: Jon Ong
Description: Create the audio clips and buttons and handle logic for Harmonium
*/

(function() {
	var clipDur = 0.3; // Clip duration in seconds

	// Helper function to generate chromatic scale; n=0 ==> 220Hz == A3
	// With a linear decrease in amplitude
	var createNote = function(context, n) {
		var sampleRate = context.sampleRate;
		var buf = context.createBuffer(1, clipDur * sampleRate, sampleRate);

		var maxAmplitude = 0.05;
		var freq = Math.pow(2, n/12.0) * 220.0;
		var factor = freq * Math.PI * 2 / sampleRate;

		var arr = buf.getChannelData(0);
		//TODO: create a better way of managing the envelope
		var attackUntil = Math.floor(0.1*context.sampleRate);
		var amplitude = 0;
		for (var i=0; i<arr.length; ++i) {
			if (i <= attackUntil) {
				amplitude = maxAmplitude*i/attackUntil;
			} else {
				amplitude = maxAmplitude*(1.0-(i-attackUntil)/(arr.length-attackUntil));
			}
			arr[i] = amplitude * Math.sin(i * factor);
		}

		return buf;
	};


	// Returns a function to play the audio clip stored in the buffer
	var makePlay = function(context, buf) {
		return function(time) {
			var source = context.createBufferSource();
			source.buffer = buf;
			source.connect(context.destination);

			source.start(time);
		};
	};


	// Generates a major pentatonic scale
	var genPentatonic = function(context, numNotes) {
		var pentatonic_scale = [];
		var p_notes = [ 0, 2, 4, 7, 9 ];

		for (var i=0, octs=0; i<numNotes; ++i) {
			if (i > 0 && i % p_notes.length == 0) {
				++octs;
			}

			var buf = createNote(context, p_notes[i % p_notes.length] + octs * 12);
			pentatonic_scale.push({
				play: makePlay(context, buf),
				playNow: function() {
					this.play(context.currentTime);
				},
			});
		}

		return pentatonic_scale;
	};


	// Do up the buttons and logic
	var finishedLoading = function(context, notes) {
		var cols = notes.length;
		var rows = cols;

		$('#status').remove();

		// Returns a function to handle a button press
		var pushButton = function(note, row, col) {
			return function() {
				note.playNow();

				$(this).toggleClass('noteon');

				boards[currentBoard][row][col] = 1 - boards[currentBoard][row][col];
			};
		};

		// Start drawing buttons!
		for (var r=0; r<rows; ++r) {
			var row = $('<div class="row"></div>').appendTo('#playground');
			for (var c=0; c<cols; ++c) {
				$('<div id="btn'+r+'_'+c+'" class="col'+c+' note note'+r+'"></div>')
					.appendTo(row)
					.click(pushButton(notes[r], r, c));
			}
		}


		// Prepare the vars needed for Conway's Game of Life
		var initBoard = function() {
			var board = [];
			for (var r=0; r<rows; ++r) {
				board.push([]);
				for (var c=0; c<cols; ++c) {
					board[r].push(0);
				}
			}
			return board;
		};
		// 2D array to access the grid buttons quickly
		var btns = (function() {
			var btns = [];
			for (var r=0; r<rows; ++r) {
				btns.push([]);
				for (var c=0; c<cols; ++c) {
					btns[r].push($('#btn'+r+'_'+c));
				}
			}
			return btns;
		})();
		var currentBoard = 0;
		var boards = [ initBoard(), initBoard() ];
		var prepareNextBoard = function() {
			var oldBuffer = boards[currentBoard];
			var buffer = boards[1-currentBoard];
			for (var r=0; r<rows; ++r) {
				var rowAbove = r == 0 ? rows - 1 : r - 1;
				var rowBelow = r + 1 == rows ? 0 : r + 1;
				for (var c=0; c<cols; ++c) {
					var colLeft = c == 0 ? cols - 1 : c - 1;
					var colRight = c + 1 == cols ? 0 : c + 1;

					var nAlive = oldBuffer[rowAbove][colLeft]
						   + oldBuffer[rowAbove][c]
						   + oldBuffer[rowAbove][colRight]
						   + oldBuffer[r][colLeft]
						   + oldBuffer[r][colRight]
						   + oldBuffer[rowBelow][colLeft]
						   + oldBuffer[rowBelow][c]
						   + oldBuffer[rowBelow][colRight];
					var cellValue = oldBuffer[r][c];
					buffer[r][c] = ((cellValue && (nAlive == 2 || nAlive == 3))
							|| (!cellValue && nAlive == 3)) ? 1 : 0;
				}
			}
		};
		var swapBoards = function() {
			currentBoard = 1 - currentBoard;
			buffer = boards[currentBoard];
			for (var r=0; r<rows; ++r) {
				for (var c=0; c<cols; ++c) {
					btns[r][c].toggleClass('noteon', buffer[r][c] ? true : false);
				}
			}
		};


		// Plays the full sequence
		var isPlaying = false;
		var colPlaying = 0;
		var interval = clipDur * 1000;
		var useGol = true;
		var doSwap = false;
		var audioStartTime = 0;
		var audioNextTime = 0;
		var timerStartTime = 0;
		var round = 1;
		var playTune = function() {
			if (audioStartTime == 0) {
				audioStartTime = context.currentTime;
				timerStartTime = new Date().getTime();
				round = 1;
			}

			$('.noteplaying').toggleClass('noteplaying', false);
			if (isPlaying) {
				if (doSwap) {
					doSwap = false;
					swapBoards();
				}

				var col = colPlaying;
				for (var i=0; i<rows; ++i) {
					if (btns[i][col].hasClass('noteon')) {
						// Play notes based on relative time for better timing
						notes[i].play(audioNextTime);
					}
				}
				$('.col'+col).toggleClass('noteplaying', true);

				++colPlaying;
				if (colPlaying >= cols) {
					colPlaying = 0;

					if (useGol) {
						prepareNextBoard();
						doSwap = true;
					}
				}

				audioNextTime = audioStartTime + round * clipDur;
				var timerNextTime = timerStartTime + round * interval;
				// Give the timeout a 50ms leeway
				var delay = Math.max(timerNextTime - new Date().getTime() - 50, 0);
				window.setTimeout(playTune, delay);

				++round;
			} else {
				doSwap = false;
				audioStartTime = 0;
				audioNextTime = 0;
				timerStartTime = 0;
			}
		};

		$('<button>Play</button>').appendTo('#controls').click(function() {
			$(this).toggleClass('playing');
			if (!isPlaying) {
				colPlaying = 0;
				isPlaying = true;

				$(this).text('Stop');
				playTune();
			} else {
				isPlaying = false;
				$(this).text('Play');
			}
		});

		$('<input type="checkbox" name="usegol" checked="true">').prependTo(
			$('<label>Enable Conway\'s Game of Life mode</label>').appendTo('#controls'))
			.change(function() {
				useGol = $(this).attr('checked');
			});
	};



	var init = function() {
		$('#playground').append('<div id="status">Loading</div>');

		try {
			var audioContext;
			if (typeof AudioContext !== "undefined") {
				audioContext = new AudioContext();
			} else if (typeof webkitAudioContext !== "undefined") {
				audioContext = new webkitAudioContext();
			} else {
				throw new Error('AudioContext not supported.');
			}

			finishedLoading(audioContext, genPentatonic(audioContext, 16));

		} catch (e) {
			$('#status').html('<p>An error occurred while creating the audio clips. Either the Web Audio API is not supported in this browser, or something funky is going on.</p><p>Details: ' + e + '</p>');
		}
	};


	$(init);
})();
